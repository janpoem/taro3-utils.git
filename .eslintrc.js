module.exports = {
  parser        : '@typescript-eslint/parser',
  plugins       : [
    '@typescript-eslint',
  ],
  extends       : [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
  ],
  env           : {
    node : true,
    mocha: true,
  },
  ignorePatterns: [
    'node_modules', 'lib'
  ]
};
