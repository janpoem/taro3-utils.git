# Taro3 辅助库

## 介绍

提供 taroConfig 和 CustomProjectConfig 以辅助 Taro 开发。

整合 [taro3-config-helper](https://gitee.com/janpoem/taro3-config-helper)
和 [taro-custom-project-config-plugin](https://www.npmjs.com/package/taro-custom-project-config-plugin)  ，并用 typescript
重构。

__taroConfig__

- 为项目提供更简易的方式创建 Taro 项目的配置，即可用于多项目模式，也可用于单项目模式。
- 提供 npm 命令解析以提取出相应的 项目名称、版本号、运行模式、自定义运行环境，包含两种模式：
  - 基于 npm run 指令解析提取，如：npm run project-name:platform:mode:custom-env
  - 基于 npm run 指令内容提取，如：npm run anything => `cross-env PROJECT=project MODE=dev taro build --type weapp`
- 提供对应 callback 可重载 项目根目录、源代码根目录、输出根目录、输出文件夹名称。

__CustomProjectConfig__

基于 [taro 插件机制](http://taro-docs.jd.com/taro/docs/miniprogram-plugin/) 开发，无缝嵌入 taro 流程。

提供根据项目的 运行模式（development or production） 和 自定义运行环境，加载不同小程序配置文件，并监控该文件的改变。

该插件主要为了解决多人合作式开发时，不同开发者的项目配置文件之间的冲突合并问题。

同时也为了将 PROD 模式的小程序配置文件彻底固化，能更高效的进行小程序的编译和发布。

## 安装教程

```shell
npm i taro3-utils --save-dev
yarn add taro3-utils -D
```

## 使用说明

```javascript
const { resolve } = require('path');
const { taroConfig, customProjectConfig, deepMerge, ArrayMergeMode } = require('taro3-utils');
// ArrayMergeMode = COMBINE | REPLACE | MERGE
// arrayMode.default = ArrayMergeMode.MERGE

// others export methods
// const { isExistDir, isExistFile, isEmptyString, filterExistFiles } = require('taro3-utils');
// const { extractProjectInfo, parseNpmCommand, RuntimeMode, filterRuntimeMode } = require('taro3-utils');

module.exports = function (merge) {
  const { config, info } = taroConfig({
    root  : 'any-path',
    config: deepMerge([
      require('./base-config'),
      info.mode === 'dev' ? require('./dev') : require('./prod'),
    ], { arrayMode: ArrayMergeMode.COMBINE })
  });

  config.plugins.push(
    customProjectConfig({
      configRoot: resolve(info.projectRoot, 'miniprogram'),
      outputRoot: info.outputRoot,
    })
  );

  return config;
};
```

在项目中创建 `miniprogram` 目录，添加对应平台的小程序配置文件：

```
weapp.json
weapp.dev.json
weapp.prod.json
weapp.dev.user.json
tt.json
```

假定执行的指令为：`npm run any-project:weapp:dev:jacky` ，配置文件列表检索顺序如下（先匹配文件存在的则作为当前环境的小程序配置文件）：

- weapp.dev.jacky.json
- weapp.jacky.json
- weapp.dev.json
- weapp.json

__注意__

1. 多项目环境，需要在项目根目录添加 `babel.config.js` 和 `tsconfig.json` 文件
2. `global.d.ts` 文件，添加如下内容：

```typescript
// taro 原有内容
declare module '*.png';
declare module '*.gif';
declare module '*.jpg';
declare module '*.jpeg';
declare module '*.svg';
declare module '*.css';
declare module '*.less';
declare module '*.scss';
declare module '*.sass';
declare module '*.styl';

declare namespace NodeJS {
  interface ProcessEnv {
    TARO_ENV: 'weapp' | 'swan' | 'alipay' | 'h5' | 'rn' | 'tt' | 'quickapp' | 'qq' | 'jd'
  }
}

// 添加全局常量
declare const AppName: string;
declare const AppVersion: string;
declare const AppMode: 'dev' | 'prod';
declare const AppPlatform: 'weapp' | 'swan' | 'alipay' | 'h5' | 'rn' | 'tt' | 'quickapp' | 'qq' | 'jd';
declare const AppCustom: string;

```
