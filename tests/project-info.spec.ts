import { assert } from 'chai';
import { extractProjectInfo, filterRuntimeMode, parseNpmCommand } from '../src';

describe('project-info', () => {
  describe('filterRuntimeMode', () => {
    it('example 1', () => {
      const modes = [
        'a', 'devel', 'prod', 'dev', 'development',
      ];
      const result = modes.map(filterRuntimeMode);
      const except = ['dev', 'dev', 'prod', 'dev', 'dev'];
      assert.deepEqual(result, except);
    });
  });

  describe('parseNpmCommand', () => {
    const tests = [
      {
        name   : '4 segments',
        command: 'my-app:weapp:dev:test',
        except : {
          project : 'my-app',
          platform: 'weapp',
          mode    : 'dev',
          custom  : 'test',
        },
      },
      {
        name   : '3 segments',
        command: 'my-app:h5:prod',
        except : {
          project : 'my-app',
          platform: 'h5',
          mode    : 'prod',
          custom  : '',
        },
      },
      {
        name   : '2 segments',
        command: 'hello-world:jd',
        except : {
          project : 'hello-world',
          platform: 'jd',
          mode    : 'dev',
          custom  : '',
        },
      },
    ];

    tests.forEach(({ name, command, except }) => {
      it(name, () => {
        assert.deepEqual(parseNpmCommand(command), except);
      });
    });
  });

  describe('extractProjectInfo - by npm_lifecycle_event', () => {
    const tests = [
      {
        name  : 'npm_lifecycle_event 4 segments',
        env   : {
          npm_lifecycle_event: 'my-app:weapp:dev:test',
        },
        except: {
          project : 'my-app',
          platform: 'weapp',
          mode    : 'dev',
          custom  : 'test',
        },
      },
      {
        name  : 'npm_lifecycle_event 3 segments',
        env   : {
          npm_lifecycle_event: 'my-app:h5:prod',
        },
        except: {
          project : 'my-app',
          platform: 'h5',
          mode    : 'prod',
          custom  : '',
        },
      },
      {
        name  : 'npm_lifecycle_event 2 segments',
        env   : {
          npm_lifecycle_event: 'hello-world:jd',
        },
        except: {
          project : 'hello-world',
          platform: 'jd',
          mode    : 'dev',
          custom  : '',
        },
      },
    ];

    tests.forEach(({ name, env, except }) => {
      it(name, () => {
        assert.deepEqual(extractProjectInfo(env), except);
      });
    });
  });

  describe('extractProjectInfo - cross env', () => {
    const tests = [
      {
        name  : 'cross env 4 segments',
        env   : {
          npm_lifecycle_event: 'my-app:weapp:dev:test',
          TARO_ENV           : 'h5',
          CUSTOM             : 'jacky',
        },
        except: {
          project : 'my-app',
          platform: 'h5',
          mode    : 'dev',
          custom  : 'jacky',
        },
      },
      {
        name  : 'cross env 3 segments',
        env   : {
          npm_lifecycle_event: 'my-app:h5:prod',
          CUSTOM             : 'test',
          MODE               : 'dev',
          PROJECT            : 'good-guy',
        },
        except: {
          project : 'good-guy',
          platform: 'h5',
          mode    : 'dev',
          custom  : 'test',
        },
      },
      {
        name  : 'cross env 2 segments',
        env   : {
          npm_lifecycle_event: 'hello-world:jd',
          PROJECT            : 'no-good',
          MODE               : 'prod',
        },
        except: {
          project : 'no-good',
          platform: 'jd',
          mode    : 'prod',
          custom  : '',
        },
      },
    ];

    tests.forEach(({ name, env, except }) => {
      it(name, () => {
        assert.deepEqual(extractProjectInfo(env), except);
      });
    });
  });
});
