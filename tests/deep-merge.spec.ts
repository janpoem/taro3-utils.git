import { assert } from 'chai';
import { deepMerge, ArrayMergeMode } from '../src';

describe('deepMerge', () => {
  it('array replace', () => {
    const result = deepMerge([
      [0, 1, 2, 3, 4],
      ['a', 'b', 'c', 'd', 'e'],
    ], { arrayMode: ArrayMergeMode.REPLACE });
    const except = ['a', 'b', 'c', 'd', 'e'];
    assert.deepEqual(result, except);
  });

  it('array combine', () => {
    const result = deepMerge([
      [0, 1, 2, 3, 4],
      ['a', 'b', 'c', 'd', 'e'],
    ], { arrayMode: ArrayMergeMode.COMBINE });
    const except = [
      0, 1, 2, 3, 4,
      'a', 'b', 'c', 'd', 'e',
    ];
    assert.deepEqual(result, except);
  });

  it('array merge', () => {
    const result = deepMerge([
      [0, 1, 2, 3, 4],
      ['a', 'b', 'c', 'd'],
    ], { arrayMode: ArrayMergeMode.MERGE });
    const except = [
      'a', 'b', 'c', 'd', 4,
    ];
    assert.deepEqual(result, except);
  });
});
